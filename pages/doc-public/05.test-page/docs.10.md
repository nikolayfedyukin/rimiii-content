---
title: 'Test page'
media_order: 'USBIO_16_8_user_manual_rus.pdf,Add_file.png'
taxonomy:
    category:
        - docs
    section:
        - tools
        - spectools
---

Пример прикрепленного файла:[USBIO_16_8_user_manual_rus.pdf](USBIO_16_8_user_manual_rus.pdf)

#### Добавление файлов через админ панель
Для добавления файла к топику через админ.панель нужно перетянуть файл в область в нижней части экрана (1), после этого перетащить в нужное место файла (2).![](Add_file.png)